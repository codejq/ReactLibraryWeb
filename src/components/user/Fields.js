export const fields = (entity, handleInputChange) => [
    {
      label: 'Nombres',
      name: 'firstName',
      type: 'text',
      value: entity.firstName,
      onChange: handleInputChange,
    },
    {
      label: 'Apellidos',
      name: 'lastName',
      type: 'text',
      value: entity.lastName,
      onChange: handleInputChange,
    },
    {
      label: 'Dirección',
      name: 'address',
      type: 'text',
      value: entity.address,
      onChange: handleInputChange,
    },
    {
      label: 'Correo Electrónico',
      name: 'email',
      type: 'email',
      value: entity.email,
      onChange: handleInputChange,
    },
    {
      label: 'Teléfono',
      name: 'phoneNumber',
      type: 'text',
      value: entity.phoneNumber,
      onChange: handleInputChange,
    }
  ];

export const columns = [
    { header: 'ID', accessor: 'id' },
    { header: 'Nombre', accessor: 'fullName' },
    { header: 'Correo Electrónico', accessor: 'email' },
    { header: 'Teléfono', accessor: 'phoneNumber' }
];

export const jsonObject =
{
  "id": 0,
  "firstName": null,
  "lastName": null,
  "address": null,
  "email": null,
  "phoneNumber": null,
  "fullName": null
};

export const fillItem = (item) => ( 
  { 
    "id": item.id,
    "firstName": item.firstName,
    "lastName": item.lastName,
    "address": item.address,
    "email": item.email,
    "phoneNumber": item.phoneNumber,
    "fullName": item.fullName 
  }
);