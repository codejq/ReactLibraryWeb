import React, { useState, useEffect } from 'react';
import { Table, Button } from 'react-bootstrap';
import ReactPaginate from 'react-paginate';
import axios from 'axios';
import lodash from 'lodash';

const GenericCRUD = ({ endpoint, columns, refreshData, onEdit, onDelete, onAddNew }) => {
  const [data, setData] = useState([]);
  const [currentPage, setCurrentPage] = useState(0);
  const [perPage] = useState(10);
  const [totalPages, setTotalPages] = useState(0);

  useEffect(() => {
    // Se ejecuta el API para obtener el listado de datos y se coloca calcula el pagineo.
    const fetchData = async () => {
      try {
        const response = await axios.get(endpoint);
        setData(response.data);
        setTotalPages(Math.ceil(response.data.length / perPage));
      } catch (error) {
        console.error('Error fetching data:', error);
      }
    };

    fetchData();

  }, [endpoint, refreshData]);

  const handleEdit = (item) => {
    if (onEdit) {
      onEdit(item);
    }
  };

  const handleDelete = (itemId) => {
    if (onDelete) {
      onDelete(itemId);
    }
  };

  const renderRows = () => {
    return data
      .slice(currentPage * perPage, (currentPage + 1) * perPage)
      .map((item) => (
        <tr key={item.id}>
          {columns.map((col, index) => (
            <td key={index}>{ lodash.get(item, col.accessor.toString()) }</td>
          ))}
          <td>
            <Button variant="warning" onClick={() => handleEdit(item)}>Editar</Button>{' '}
            <Button variant="danger" onClick={() => handleDelete(item.id)}>Eliminar</Button>
          </td>
        </tr>
      ));
  };

  return (
    <div>
      <div className="d-flex justify-content-end mb-3">
        <Button onClick={onAddNew} variant="primary">Agregar Nuevo</Button>
      </div>

      <Table striped bordered hover>
        <thead>
          <tr>
            {columns.map((col, index) => (
              <th key={index}>{col.header}</th>
            ))}
          </tr>
        </thead>
        <tbody>{renderRows()}</tbody>
      </Table>

      <ReactPaginate
        pageCount={totalPages}
        onPageChange={(data) => setCurrentPage(data.selected)}
        containerClassName={'pagination'}
        activeClassName={'active'}
      />
    </div>
  );
};

export default GenericCRUD;