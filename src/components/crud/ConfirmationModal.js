import React from "react"
import { Modal, Button } from 'react-bootstrap';
import axios from "axios";


const ConfimationModal = ({ endpoint, show, onHide, title, msg, onFinished }) => {

  // Se ejecuta el metodo de borrado por medio del API
  const deleteItem = async () => {
    try {
      await axios.delete(endpoint);
      // Se ejecuta el metodo de finalizacion del proceso que debe actualizar la lista y cerrar el modal
      onFinished();
    } catch (error) {
      console.error('Error deleting genre:', error);
    }
  };
  
  return (
    <Modal show={show} onHide={onHide}>
      <Modal.Header closeButton>
        <Modal.Title>{title}</Modal.Title>
      </Modal.Header>
      <Modal.Body>
        {msg}
      </Modal.Body>
      <Modal.Footer>
        <Button variant="secondary" onClick={onHide}>Cancelar</Button>
        <Button variant="danger" onClick={deleteItem}>Eliminar</Button>
      </Modal.Footer>
    </Modal>
  );
};

export default ConfimationModal;