import React, { useState, useEffect } from 'react';
import GenericForm from '../crud/GenericForm'; 
import axios from 'axios';
import lodash from 'lodash';

const EditItemForm = ({ endpoint, item, onItemUpdated, fields, jsonObject, fillItem }) => {
  const [editItem, setEditItem] = useState(jsonObject);

  useEffect(() => {
    if (item) {
      // Al cargar el formulario se llena automaticamente con los valores del objeto JSON.
      setEditItem(fillItem(item))
    }
  }, [item]); 

  const handleInputChange = (e) => {
    const { name, value } = e.target; // Se obtiene el nombre del campo y el nuevo valor
    const updatedItem = { ...editItem }; // Se hace una copia para inmutabilidad
    lodash.set(updatedItem, name, value); // Se actualiza el objeto segun la ruta y no importa si tiene objetos anidados
    setEditItem(updatedItem); // Actualiza el estado con el objeto modificado
  };

  // Se hace la actualizacion del objeto enviandolo al API
  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.put(endpoint, editItem);
      // Se ejecuta el metodo de finalizacion del proceso que debe actualizar la lista y cerrar el modal
      onItemUpdated(); 
    } catch (error) {
      console.error('Error updating item:', error);
    }
  };


  return (
    <GenericForm fields={fields(editItem, handleInputChange)} onSubmit={handleSubmit} />
  );
};

export default EditItemForm;
