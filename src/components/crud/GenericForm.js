import React from 'react';
import { Form, Button } from 'react-bootstrap';

// Componente genérico para formularios
const GenericForm = ({ fields, onSubmit }) => (

  <Form onSubmit={onSubmit}>
    {fields.map((field, index) => (
      <Form.Group key={index} controlId={field.name}>
        <Form.Label>{field.label}</Form.Label>
        {field.type === 'select' ? (
          <Form.Control
            as="select" // Si es un campo de selección
            name={field.name}
            value={field.value}
            onChange={field.onChange}
            className="form-select"
          >
            {field.options.map((option) => (
              <option key={option.value} value={option.value}>
                {option.label}
              </option>
            ))}
          </Form.Control>
        ) : (
          <Form.Control
            type={field.type} // Otros tipos de campo
            name={field.name}
            value={field.value}
            onChange={field.onChange}
          />
        )}
      </Form.Group>
    ))}
    <br/>
    <Button type='submit' variant="danger">Guardar</Button>
  </Form>
);

export default GenericForm;
