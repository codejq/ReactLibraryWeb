import React from 'react';
import { Modal, Button } from 'react-bootstrap';

const GenericModal = ({ show, onHide, title, children }) => (
  <Modal show={show} onHide={onHide}>
    <Modal.Header closeButton>
      <Modal.Title>{title}</Modal.Title>
    </Modal.Header>
    <Modal.Body>
       {/* Contenido del modal */}
      {children}
    </Modal.Body>
  </Modal>
);

export default GenericModal;
