import React, { useState } from 'react';
import GenericTable from '../crud/GenericTable';
import GenericModal from '../crud/GenericModal';
import NewGenericForm from '../crud/NewGenericForm';
import EditGenreForm from '../crud/EditGenericForm';
import ConfirmationModal from '../crud/ConfirmationModal';

const GenericCRUD = (props) => {
    // Destructuring para extraer las propiedades
    const { apiEntityList, apiEntityNew, apiEntityEdit, apiEntityDelete, fields, columns, titulo, jsonObject, fillItem } = props;
    const [showNewGenreModal, setShowNewGenreModal] = useState(false);
    const [showEditGenreModal, setShowEditGenreModal] = useState(false);
    const [showDeleteGenreModal, setShowDeleteGenreModal] = useState(false);
    const [editGenre, setEditGenre] = useState(null);
    const [selectedItem, setSelectedItem] = useState(null);
    const [refreshData, setRefreshData] = useState(false);

    const handleAddNew = () => {
        setShowNewGenreModal(true);
    };

    const handleEdit = (item) => {
        setEditGenre(item);
        setShowEditGenreModal(true);
    };

    const handleDelete = (itemId) => {
        setSelectedItem(itemId);
        setShowDeleteGenreModal(true);
    };

    return (
        <div>
            <h1>{titulo}</h1>
            {/* Tabla con listado de datos que contiene columna de acciones con boton de editar y eliminar. */}
            <GenericTable
                endpoint={apiEntityList}
                columns={columns}
                refreshData={refreshData}
                onEdit={handleEdit}
                onDelete={handleDelete}
                onAddNew={handleAddNew}
            />

            {/* Modal para agregar nuevos registros */}
            <GenericModal
                show={showNewGenreModal}
                onHide={() => setShowNewGenreModal(false)}
                title="Agregar Nuevo"
            >
                <NewGenericForm
                    endpoint={apiEntityNew}
                    jsonObject={jsonObject}
                    fields={fields}
                    onItemAdded={() => {
                        setRefreshData((prev) => !prev);
                        setShowNewGenreModal(false);
                    }}
                />
            </GenericModal>

            {/* Modal para editar */}
            <GenericModal
                show={showEditGenreModal}
                onHide={() => setShowEditGenreModal(false)}
                title="Editar"
            >
                {editGenre && (
                    <EditGenreForm
                        endpoint={apiEntityEdit}
                        jsonObject={jsonObject}
                        fields={fields}
                        item={editGenre}
                        fillItem={fillItem}
                        onItemUpdated={() => {
                            setRefreshData((prev) => !prev);
                            setShowEditGenreModal(false);
                        }}
                    />
                )}
            </GenericModal>

            {/* Modal de confirmación para eliminar */}
            <ConfirmationModal
                endpoint={apiEntityDelete + `/${selectedItem}`}
                show={showDeleteGenreModal}
                onHide={() => setShowDeleteGenreModal(false)}
                title="Confirmar eliminación"
                msg="¿Estás seguro de que deseas eliminar este registro?"
                onFinished={() => {
                    setRefreshData((prev) => !prev);
                    setShowDeleteGenreModal(false);
                }}
            />
        </div>
    );
};

export default GenericCRUD;
