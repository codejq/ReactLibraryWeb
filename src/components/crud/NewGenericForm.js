import React, { useState } from 'react';
import axios from 'axios';
import GenericForm from '../crud/GenericForm'; 
import lodash from 'lodash';

const NewGenericForm = ({endpoint, onItemAdded, fields, jsonObject}) => {
  
  const [newItem, setNewItem] = useState(jsonObject);

  const handleInputChange = (e) => {
    const { name, value } = e.target; // Se obtiene el nombre del campo y el nuevo valor
    const item = { ...newItem }; // Se hace una copia para inmutabilidad
    lodash.set(item, name, value);  // Se actualiza el objeto segun la ruta y no importa si tiene objetos anidados
    setNewItem(item); // Actualiza el estado con el objeto modificado
  };

  const handleSubmit = async (e) => {
    e.preventDefault();
    try {
      await axios.post(endpoint, newItem);  
      onItemAdded();
    } catch (error) {
      console.error('Error adding genre:', error);
    }
  };

  return (
    <GenericForm fields={ fields(newItem, handleInputChange) } onSubmit={handleSubmit} />
  );
};

export default NewGenericForm;
