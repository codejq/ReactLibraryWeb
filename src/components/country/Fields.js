export const fields = (entity, handleInputChange) => [
  {
    label: 'Nombre',
    name: 'name',
    type: 'text',
    value: entity.name,
    onChange: handleInputChange,
  },
  {
    label: 'Código de Área',
    name: 'areaCode',
    type: 'text',
    value: entity.areaCode,
    onChange: handleInputChange,
  },
  {
    label: 'Código ISO2',
    name: 'isoCode',
    type: 'text',
    value: entity.isoCode,
    onChange: handleInputChange,
  }
];

export const columns = [
  { header: 'ID', accessor: 'id' },
  { header: 'Nombre', accessor: 'name' },
  { header: 'Código ISO 2', accessor: 'isoCode' }
];

export const jsonObject =
{
  "id": 0,
  "name": null,
  "areaCode": null,
  "isoCode": null
};

export const fillItem = (item) => ( 
  { 
    "id": item.id, 
    "name": item.name, 
    "isoCode": item.isoCode, 
    "areaCode": item.areaCode 
  } 
);