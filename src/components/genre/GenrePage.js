import React from 'react';
import GenericCRUD from '../crud/GenericCRUD';
import { fields, columns, jsonObject, fillItem } from './Fields';

const EntityPage = () => {
  const apiUrl = process.env.REACT_APP_BACKEND;
  const apiName = "Genres";
  const titulo = "Géneros";
  const getAll = "GetGenres";

  return (
    // Agrega la tabla que contiene las opciones de agregar, editar o borrar.
    <GenericCRUD
      titulo={titulo}
      fields={fields}
      columns={columns}
      jsonObject={jsonObject}
      fillItem={fillItem}
      apiEntityList={`${apiUrl}/api/${apiName}/${getAll}`}
      apiEntityNew={`${apiUrl}/api/${apiName}/Registry`}
      apiEntityEdit={`${apiUrl}/api/${apiName}/Modify`}
      apiEntityDelete={`${apiUrl}/api/${apiName}/Remove`}
    />
  );
};

export default EntityPage;
