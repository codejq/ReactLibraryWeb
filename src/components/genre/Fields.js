export const fields = (entity, handleInputChange) => [
    {
      label: 'Nombre',
      name: 'name',
      type: 'text',
      value: entity.name,
      onChange: handleInputChange,
    }
  ];

export const columns = [
    { header: 'ID', accessor: 'id' },
    { header: 'Nombre', accessor: 'name' },
];

export const jsonObject =
{
  "id": 0,
  "name": ''
};

export const fillItem = (item) => ( 
  { 
    "id": item.id, 
    "name": item.name  
  } 
);