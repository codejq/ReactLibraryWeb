import React, {useEffect, useState} from 'react';
import GenericCRUD from '../crud/GenericCRUD';
import { fields as defaultFields, columns, jsonObject, fillItem } from './Fields';
import axios from 'axios';

const EntityPage = () => {
  const apiUrl = process.env.REACT_APP_BACKEND; 
  const apiName = "Authors";
  const titulo="Autores";
  const getAll="GetAuthors";
  
  // Estado para almacenar catalogo de paises
  const [countries, setCountries] = useState([]);

  useEffect(() => {
    const fetchCountries = async () => {
      try{
        // Promesa
        const response = await axios.get(`${apiUrl}/api/Country/GetCountries`);
        setCountries(response.data);
      }catch(error){
        console.log('Error log: ', error);
      }
    };

    fetchCountries();
  }, []);

  // Al finalizar la carga de paises, carga el resultado del catalogo a Fields
  const fields = (entity, handleInputChange) => defaultFields(entity, handleInputChange, countries);

  return (
    // Agrega la tabla que contiene las opciones de agregar, editar o borrar.
    <GenericCRUD
    titulo={titulo}
    fields={fields}
    columns={columns}
    jsonObject={jsonObject}
    fillItem={fillItem}
    apiEntityList={`${apiUrl}/api/${apiName}/${getAll}`} 
    apiEntityNew={`${apiUrl}/api/${apiName}/Registry`} 
    apiEntityEdit={`${apiUrl}/api/${apiName}/Modify`} 
    apiEntityDelete={`${apiUrl}/api/${apiName}/Remove`}  
    />
  );
};

export default EntityPage;