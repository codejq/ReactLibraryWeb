const NA_REQUIRED = "";

export const fields = (entity, handleInputChange, countries) => [
  {
    label: 'Nombre',
    name: 'firstName',
    type: 'text',
    value: entity?.firstName ?? '',
    onChange: handleInputChange,
  },
  {
    label: 'Apellidos',
    name: 'lastName',
    type: 'text',
    value: entity?.lastName ?? '',
    onChange: handleInputChange,
  },
  {
    label: 'Fecha de Nacimiento',
    name: 'birthday',
    type: 'date',
    value: entity?.birthday ?? '',
    onChange: handleInputChange,
  },
  {
    label: 'Pais',
    name: 'country.id', // Se coloca el objeto anidado y su identificador
    type: 'select',
    value: entity.country?.id ?? '',
    onChange: handleInputChange,
    options: countries.map((country) => ({ value: country.id, label: country.name })) // Carga las opciones de forma dinamica
  }
];

export const columns = [
  { header: 'ID', accessor: 'id' },
  { header: 'Nombre', accessor: 'fullName' },
  { header: 'País', accessor: 'country.name' },
];

export const jsonObject =
{
  "id": 0,
  "firstName": null,
  "lastName": null,
  "birthday": null,
  "country": {
    "id": null,
    "name": NA_REQUIRED,
    "areaCode": NA_REQUIRED,
    "isoCode": NA_REQUIRED
  },
  "fullName": NA_REQUIRED
};

export const fillItem = (item) => ({ 
  "id": item.id,
  "firstName": item.firstName,
  "lastName": item.lastName,
  "birthday": item.birthday,
  "country": {
    "id": item.country.id ,
    "name": NA_REQUIRED,
    "areaCode": NA_REQUIRED,
    "isoCode": NA_REQUIRED
  },
  "fullName": NA_REQUIRED
});