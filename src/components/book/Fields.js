const NA_REQUIRED = "";

export const fields = (entity, handleInputChange, authors, genres) => [
  {
    label: 'Título',
    name: 'title',
    type: 'text',
    value: entity?.title ?? '',
    onChange: handleInputChange,
  },
  {
    label: 'Año de Publicación',
    name: 'publicationYear',
    type: 'number',
    value: entity?.publicationYear ?? '',
    onChange: handleInputChange,
  }, 
  {
    label: 'Autor',
    name: 'author.id',  // Se coloca el objeto anidado y su identificador
    type: 'select',
    value: entity.author?.id ?? '',
    onChange: handleInputChange,
    options: authors.map((item) => ({value: item.id, label: item.fullName}))
  },
  {
    label: 'Género',
    name: 'genre.id',  // Se coloca el objeto anidado y su identificador
    type: 'select',
    value: entity.genre?.id ?? '',
    onChange: handleInputChange,
    options: genres.map((item) => ({value: item.id, label: item.name}))
  }
];

export const columns = [
  { header: 'ID', accessor: 'id' },
  { header: 'Título', accessor: 'title' },
  { header: 'Publicación', accessor: 'publicationYear' },
  { header: 'Autor', accessor: 'author.fullName' },
];

export const jsonObject =
{
  "id": 0,
  "title": null,
  "publicationYear": null,
  "author": {
    "id": 0,
    "firstName": NA_REQUIRED,
    "lastName": NA_REQUIRED,
    "birthday": "1900-01-01",
    "country": {
      "id": 0,
      "name": NA_REQUIRED,
      "areaCode": NA_REQUIRED,
      "isoCode": NA_REQUIRED
    },
    "fullName": NA_REQUIRED
  },
  "genre": {
    "id": 0,
    "name": NA_REQUIRED
  }
};

export const fillItem = (item) => ({
  "id": item.id,
  "title": item.title,
  "publicationYear": item.publicationYear,
  "author": {
    "id": item.author?.id,
    "firstName": NA_REQUIRED,
    "lastName": NA_REQUIRED,
    "birthday": "1900-01-01",
    "country": {
      "id": 0,
      "name": NA_REQUIRED,
      "areaCode": NA_REQUIRED,
      "isoCode": NA_REQUIRED
    },
    "fullName": NA_REQUIRED
  },
  "genre": {
    "id": item.genre?.id,
    "name": NA_REQUIRED
  }
});
