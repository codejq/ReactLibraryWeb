import React, { useEffect, useState } from 'react';
import GenericCRUD from '../crud/GenericCRUD';
import { fields as defaultFields, columns, jsonObject, fillItem } from './Fields';
import axios from 'axios';

const EntityPage = () => {
  const apiUrl = process.env.REACT_APP_BACKEND;
  const apiName = "Books";
  const titulo = "Libros";
  const getAll = "GetBooks";
  // Estados de catalogos
  const [authors, setAuthors] = useState([]);
  const [genres, setGenres] = useState([]);

  useEffect(() => {
    const fetchCatalogs = async () => {
      try {
        // Catalogo de autores
        const responseAuthors = await axios.get(`${apiUrl}/api/Authors/GetAuthors`);
        setAuthors(responseAuthors.data);

        // Catalogo de generos
        const responseGenres = await axios.get(`${apiUrl}/api/Genres/GetGenres`);
        setGenres(responseGenres.data);
        
      } catch (error) {
        console.error("Error msg: ", error);
      }
    };

    fetchCatalogs();
  }, []);

  const fields = (entity, handleInputChange) => defaultFields(entity, handleInputChange, authors, genres );

  return (
    // Agrega la tabla que contiene las opciones de agregar, editar o borrar.
    <GenericCRUD
      titulo={titulo}
      fields={fields}
      columns={columns}
      jsonObject={jsonObject}
      fillItem={fillItem}
      apiEntityList={`${apiUrl}/api/${apiName}/${getAll}`}
      apiEntityNew={`${apiUrl}/api/${apiName}/Registry`}
      apiEntityEdit={`${apiUrl}/api/${apiName}/Modify`}
      apiEntityDelete={`${apiUrl}/api/${apiName}/Remove`}
    />
  );
};

export default EntityPage;