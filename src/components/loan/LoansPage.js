import React, { useEffect, useState } from 'react';
import GenericCRUD from '../crud/GenericCRUD';
import { fields as defaultFields, columns, jsonObject, fillItem } from './Fields';
import axios from 'axios';

const EntityPage = () => {
    const apiUrl = process.env.REACT_APP_BACKEND;
    const apiName = "Loans";
    const titulo = "Préstamos";
    const getAll = "GetLoans";
    // Estados para catalogos
    const [users, setUsers] = useState([]);
    const [books, setBooks] = useState([]);

    useEffect(() => {
        const fetchCatalogs = async () => {
            try{
                const catalogUsers = await axios.get(`${apiUrl}/api/Users/GetUsers`);
                setUsers(catalogUsers.data);
                const catalogBooks = await axios.get(`${apiUrl}/api/Books/GetBooks`);
                setBooks(catalogBooks.data);
            }
            catch(error) {
                console.log("Error msg: ", error);
            }
        };

        fetchCatalogs();
    }, []);

    const fields = (entity, handleInputChange) => defaultFields(entity, handleInputChange, users, books);

    return (
        // Agrega la tabla que contiene las opciones de agregar, editar o borrar.
        <GenericCRUD
            titulo={titulo}
            fields={fields}
            columns={columns}
            jsonObject={jsonObject}
            fillItem={fillItem}
            apiEntityList={`${apiUrl}/api/${apiName}/${getAll}`}
            apiEntityNew={`${apiUrl}/api/${apiName}/Registry`}
            apiEntityEdit={`${apiUrl}/api/${apiName}/Modify`}
            apiEntityDelete={`${apiUrl}/api/${apiName}/Remove`}
        />
    );
};

export default EntityPage;