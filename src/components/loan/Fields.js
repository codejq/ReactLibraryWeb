const NA_REQUIRED = "";

export const fields = (entity, handleInputChange, users, books) => [
  {
    label: 'Fecha de Préstamo',
    name: 'loanDate',
    type: 'date',
    value: entity?.loanDate ?? '',
    onChange: handleInputChange,
  },
  {
    label: 'Fecha de Devolución',
    name: 'returnDate',
    type: 'date',
    value: entity?.returnDate ?? '',
    onChange: handleInputChange,
  },
  {
    label: 'Usuario',
    name: 'user.id',
    type: 'select',
    value: entity.user?.id ?? '',
    onChange: handleInputChange,
    options: users.map((item) => ({ value: item.id, label: item.fullName }))
  },
  {
    label: 'Libro',
    name: 'book.id',
    type: 'select',
    value: entity.book?.id ?? '',
    onChange: handleInputChange,
    options: books.map((item) => ({ value: item.id, label: item.title }))
  }
];

export const columns = [
  { header: 'ID', accessor: 'id' },
  { header: 'Fecha de Préstamo', accessor: 'loanDate' },
  { header: 'Usuario', accessor: 'user.fullName' }
];

export const jsonObject =
{
  "id": 0,
  "loanDate": null,
  "returnDate": null,
  "user": {
    "id": null,
    "firstName": NA_REQUIRED,
    "lastName": NA_REQUIRED,
    "address": NA_REQUIRED,
    "email": NA_REQUIRED,
    "phoneNumber": NA_REQUIRED
  },
  "book": {
    "id": null,
    "title": NA_REQUIRED,
    "publicationYear": 0,
    "author": {
      "id": 0,
      "firstName": NA_REQUIRED,
      "lastName": NA_REQUIRED,
      "birthday": "1900-01-01",
      "country": {
        "id": 0,
        "name": NA_REQUIRED,
        "areaCode": NA_REQUIRED,
        "isoCode": NA_REQUIRED
      }
    },
    "genre": {
      "id": 0,
      "name": NA_REQUIRED
    }
  }
};

export const fillItem = (item) => (
  {
    "id": item.id,
    "loanDate": item.loanDate,
    "returnDate": item.returnDate,
    "user": {
      "id": item.user.id,
      "firstName": NA_REQUIRED,
      "lastName": NA_REQUIRED,
      "address": NA_REQUIRED,
      "email": NA_REQUIRED,
      "phoneNumber": NA_REQUIRED
    },
    "book": {
      "id": item.book.id,
      "title": NA_REQUIRED,
      "publicationYear": 0,
      "author": {
        "id": item.user.id,
        "firstName": NA_REQUIRED,
        "lastName": NA_REQUIRED,
        "birthday": "1900-01-01",
        "country": {
          "id": 0,
          "name": NA_REQUIRED,
          "areaCode": NA_REQUIRED,
          "isoCode": NA_REQUIRED
        }
      },
      "genre": {
        "id": item.book.id,
        "name": NA_REQUIRED
      }
    }
  });