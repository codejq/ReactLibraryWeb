import React from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import CountriesPage from './country/CountriesPage';
import UsersPage from './user/UsersPage';
import GenresPage from './genre/GenrePage'
import '../css/homePages.css'; // Importa el archivo CSS para estilizar el componente
import AuthorsPage from './author/AuthorsPage';
import BooksPage from './book/BooksPage';
import LoansPage from './loan/LoansPage';

const HomePage = () => {
  return (
    <Router>
      <div>
        {/* Menú de navegación con clases de Bootstrap */}
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container">
            <Link className="navbar-brand" to="/">Inicio</Link>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="navbar-nav me-auto mb-2 mb-lg-0">
                <li className="nav-item">
                  <Link className="nav-link" to="/countries">Países</Link>
                </li>
                <li className="nav-item">
                  <Link className="nav-link" to="/users">Usuarios</Link>
                </li>
                <li className='nav-item'>
                  <Link className='nav-link' to="/genre">Género</Link>
                </li>
                <li className='nav-item'>
                  <Link className='nav-link' to="/authors">Autores</Link>
                </li>
                <li className='nav-item'>
                  <Link className='nav-link' to="/books">Libros</Link>
                </li>
                <li className='nav-item'>
                  <Link className='nav-link' to="/loans">Préstamos</Link>
                </li>
              </ul>
            </div>
          </div>
        </nav>
        
        {/* Configuración de las rutas */}
        <Routes>
          <Route path="/countries" element={<CountriesPage />} />
          <Route path="/users" element={<UsersPage />} />
          <Route path="/genre" element={ <GenresPage /> } />
          <Route path="/authors" element={ <AuthorsPage /> } />
          <Route path="/books" element={ <BooksPage />} />
          <Route path="/loans" element={ <LoansPage />} />
        </Routes>
      </div>
    </Router>
  );
};

export default HomePage;
